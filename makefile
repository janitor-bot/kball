# ------------------------------------------------------------------------
# KBall makefile
# By Kronoman
# Copyright (c) 2003, 2004
# Thanks to Schwarzung for the help on making the original makefile system.
# ------------------------------------------------------------------------

# This has the target platform defined, this is modified by fix.bat or fix.sh
include target.os

# Suggested by GNU Coding Stardards
SHELL = /bin/sh

# ===============================================
# Target binary name without extension
BINARY = kball

# Source directory
SRCDIR = src

# Include directory
INCDIR = include

# Source code suffix (.c, .cpp, etc)
SRCSUF = .cpp

# Simple source code test file (must be in same dir as makefile for now) :(
# The extension will be taken from SRCSUF, don't put it!
TESTFILE = test
# ===============================================


# -----------------------------
# -- Platform specific stuff --
# -----------------------------

# ------------------
# DJGPP target
# ------------------
ifeq ($(TARGET),DJGPP)

PLATFORMDIR=djgpp

# compiler to invoque
GCC = gxx

# Binary file suffix
BINSUF = dos.exe
# object suffix
OBJSUF = .o

# If you need extra link options (like more librarys, add to LFLAGS var)
LFLAGS = -s -laldmb -ldumb -lalleg

# Compiler flags
CFLAGS = -I$(INCDIR) -Wall -O3
endif

# ------------------
# MingW32
# ------------------
ifeq ($(TARGET),MINGW32)

PLATFORMDIR=mingw32

GCC = g++

# Binary file suffix
BINSUF = _w32.exe
OBJSUF = .o

# If you need extra link options (like more librarys, add to LFLAGS var)
LFLAGS = -Wl,--subsystem,windows -s -laldmb -ldumb -lalleg

# Compiler flags
CFLAGS = -I$(INCDIR) -Wall -O3
endif

# ------------------
# Linux
# ------------------
ifeq ($(TARGET),LINUX)

PLATFORMDIR=linux

GCC = g++

# Binary file suffix
BINSUF = _linux.bin
OBJSUF = .o

# If you need extra link options (like more librarys, add to LFLAGS var)
LFLAGS = -s -laldmb -ldumb `allegro-config --libs`

# Compiler flags
CFLAGS = -I$(INCDIR) -Wall -O3
endif

# ---------------------------------
# -- Platform non-specific stuff --
# ---------------------------------

OBJDIR = obj/$(PLATFORMDIR)
BINDIR = bin

# -- The rules for build are in this file --
include makefile.all
