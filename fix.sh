#!/bin/sh
echo "KBall Source Code"
echo
echo "KBall's website: http://kball.sf.net/"

# REMEMBER TO ALTER THIS TEST TO SUIT YOUR NEEDS!!!
proc_test()
{
    # You first need to configure the platform
    if [ ! -e target.os ]; then
	echo "Before test, you first must configure your platform."
	proc_help;
    else
	echo Testing, please wait...
	make test
	
	if [ $? -eq 0 -a -e test.run ]; then
	    echo
	    echo "* SUCESS *"
	    echo "Congratulations, the test compiled!"
	    echo
		echo "NOTE: You need Allegro 4.1.15 or better to compile KBall"
	else
	    echo
	    echo "* ERROR *"
	    echo
	    echo "The compilation returned a error or can't be runned!"
	    echo "Check that:"
	    echo "(*) You have all compiler tools installed (gcc,make,etc...)"
	    echo "(*) You have Allegro 4.1.15 or better properly installed (http://alleg.sf.net/)"
		echo "(*) You have DUMB 0.9.2 or better properly installed (http://dumb.sf.net/)"
	    echo
	fi

	echo "Cleaning the test..."
	make cleantest
    fi	
}

proc_help()
{
   echo "Usage: fix platform"
   echo
   echo "Where platform is one of: djgpp, mingw32 or linux. "
   echo
   echo "NOTICE:"
   echo "You can also call: fix test"
   echo "to check if your system can compile this programs."
   echo
   echo "To compile KBall you need Allegro 4.1.15 or better, and DUMB 0.9.2 or better"
   echo "http://alleg.sf.net/"
   echo "http://dumb.sf.net/"
   echo
}

proc_fix()
{
   echo "Configuring for $1..."

   if [ "$2" != "none" ]; then
      echo "# Warning! This file will be overwritten by configuration routines!" > target.os
      echo "TARGET=$2" >> target.os
   fi
}


# prepare for the given platform.

case "$1" in
   "djgpp"   ) proc_fix "DOS (djgpp)"       "DJGPP";;
   "mingw32" ) proc_fix "Windows (Mingw32)" "MINGW32";;
   "linux"   ) proc_fix "Linux (GCC)"       "LINUX";;
   "test"    ) proc_test;;
   "help"    ) proc_help;;
   *         ) proc_help;;
esac

echo "Done!"
