// ------------------------------------------------------------------
// musiclvl.h
// System for handling level's music
// ------------------------------------------------------------------
// By Kronoman - In loving memory of my father
// Copyright (c) 2004, Kronoman
// ------------------------------------------------------------------
#ifndef MUSICLVL_H
#define MUSICLVL_H

#include <allegro.h>
#include <aldumb.h>  // DUMB : http://dumb.sf.net/

// music level loader
class CMusicLvl
{
	public:
		CMusicLvl();
		~CMusicLvl();
		
		DUH *get_music(char *filename, int index); // gets the music from file, or a default music (NULL) otherwise
		void free_memory(); // releases memory and returns to default music
	
	private:
		DUH *music_in_ram;
		DATAFILE *data_loaded;
		
		// cache stuff
		char file_loaded[1024]; // wich file we have in memory
		int index_loaded;

};

#endif
