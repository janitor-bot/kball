// -----------------------------------------------
// particle.h
// -----------------------------------------------
// Many particle types, designed to be used with
// my particle manager.
// ----------------------------------------------- 
// Developed By Kronoman - Copyright (c) 2004
// In loving memory of my father
// -----------------------------------------------

#ifndef PARTICLE_H
#define PARTICLE_H

#include <allegro.h>

#include <string>
using namespace std;

#include "partmang.h" // particle manager

// -----------------------------------------------
// Spark particle, this looks like a spark (duh)
// -----------------------------------------------
class CSparkParticle : public CBaseParticle
{
	public:
		CSparkParticle() : CBaseParticle() { scale_spark = 1; };
		CSparkParticle(float x1, float y1, float dx1, float dy1, int color1, int life1) : CBaseParticle( x1,  y1,  dx1,  dy1,  color1,  life1) {  scale_spark = 1; };
		CSparkParticle(float x1, float y1, float dx1, float dy1, int color1, int life1, int s_spark) : CBaseParticle( x1,  y1,  dx1,  dy1,  color1,  life1) {  scale_spark = s_spark; };
		~CSparkParticle() { };

		void render_particle(BITMAP *bmp, int xd, int yd);
	
	// data
	int scale_spark; // this is the scale of the spark, defaults to 1 (bigger = bigger spark)
};

// -----------------------------------------------
// Circle fill particle
// -----------------------------------------------
class CCircleParticle : public CBaseParticle
{
	public:
		CCircleParticle() : CBaseParticle() { radius = rand()%3+1; };
		CCircleParticle(float x1, float y1, float dx1, float dy1, int color1, int life1, int r) : CBaseParticle( x1,  y1,  dx1,  dy1,  color1,  life1) {  radius = r; };
		~CCircleParticle() { };

		void render_particle(BITMAP *bmp, int xd, int yd);
	
	// data
	int radius; // radius of circle
};

// -----------------------------------------------
// Rect fill particle
// -----------------------------------------------
class CRectParticle : public CBaseParticle
{
	public:
		CRectParticle() : CBaseParticle() { size = rand()%3+1; };
		CRectParticle(float x1, float y1, float dx1, float dy1, int color1, int life1, int s) : CBaseParticle( x1,  y1,  dx1,  dy1,  color1,  life1) {  size = s; };
		~CRectParticle() { };

		void render_particle(BITMAP *bmp, int xd, int yd);
	
	// data
	int size; // size of rect
};


// -----------------------------------------------
// Text particle, renders text on the particle
// -----------------------------------------------
class CTextParticle : public CBaseParticle
{
	public:
		CTextParticle() : CBaseParticle() { text_to_show = "Krono Rulez!" ; font_text = font; };
		CTextParticle(float x1, float y1, float dx1, float dy1, int color1, int life1, string txt) : CBaseParticle( x1,  y1,  dx1,  dy1,  color1,  life1) {  text_to_show = txt; font_text = font; };
		CTextParticle(float x1, float y1, float dx1, float dy1, int color1, int life1, string txt, FONT *fnt) : CBaseParticle( x1,  y1,  dx1,  dy1,  color1,  life1) {  text_to_show = txt; font_text = fnt; };
		CTextParticle(float x1, float y1, float dx1, float dy1, int color1, int life1, char *txt, FONT *fnt) : CBaseParticle( x1,  y1,  dx1,  dy1,  color1,  life1) {  text_to_show = txt; font_text = fnt; };
		~CTextParticle() {};

		void render_particle(BITMAP *bmp, int xd, int yd);
	
	// data of particle
	string text_to_show; // text to show (duh!) 
	FONT *font_text; // font of text
};

// -----------------------------------------------
// *Explosive* TEXT particle, renders text on the particle
// when the text particle deads, "explodes",
// that means that will spawn base particles (same color as text)
// -----------------------------------------------
class CExplosiveTextParticle : public CTextParticle
{
	public:
		CExplosiveTextParticle() : CTextParticle() {};
		CExplosiveTextParticle(float x1, float y1, float dx1, float dy1, int color1, int life1, string txt) : CTextParticle( x1,  y1,  dx1,  dy1,  color1,  life1, txt) { };
		CExplosiveTextParticle(float x1, float y1, float dx1, float dy1, int color1, int life1, string txt, FONT *fnt) : CTextParticle( x1,  y1,  dx1,  dy1,  color1,  life1, txt, fnt) { };
		CExplosiveTextParticle(float x1, float y1, float dx1, float dy1, int color1, int life1, char *txt, FONT *fnt) : CTextParticle( x1,  y1,  dx1,  dy1,  color1,  life1, txt, fnt) { };
		~CExplosiveTextParticle() {};


		bool update_logic(CParticleManager &particle_manager);
};

// -----------------------------------------------
// Bitmap particle -- draws a bitmap on the particle
// -----------------------------------------------
class CBitmapParticle : public CBaseParticle
{
	public:
		CBitmapParticle() : CBaseParticle() { spr = NULL; };
		CBitmapParticle(float x1, float y1, float dx1, float dy1, int color1, int life1, BITMAP *spr1) : CBaseParticle( x1,  y1,  dx1,  dy1,  color1,  life1) {  spr =  spr1; };
		~CBitmapParticle() { };

		void render_particle(BITMAP *bmp, int xd, int yd);
	
	// data
	BITMAP *spr;
};

// -----------------------------------------------
// Rotating Bitmap particle -- draws a bitmap on the particle (ideal for a debris/spark/etc)
// -----------------------------------------------
class CRotoBitmapParticle : public CBaseParticle
{
	public:
		CRotoBitmapParticle() : CBaseParticle() { spr = NULL; angle = 0.0; angle_speed = 0.0; };
		CRotoBitmapParticle(float x1, float y1, float dx1, float dy1, int color1, int life1, BITMAP *spr1, float ang, float ang_s) : CBaseParticle( x1,  y1,  dx1,  dy1,  color1,  life1) {  spr =  spr1; angle = ang; angle_speed = ang_s; };
		~CRotoBitmapParticle() { };

		void render_particle(BITMAP *bmp, int xd, int yd);
		bool update_logic(CParticleManager &particle_manager);
	
	// data
	BITMAP *spr;
	float angle;
	float angle_speed;
};


#endif
