// -----------------------------------------------
// Sound System
// By Kronoman
// Copyright (c) 2004, Kronoman
// In loving memory of my father
// -----------------------------------------------
// sound.h
// -----------------------------------------------
// This system wraps around Allegro and DUMB,
// so I can use the high level sound routines,
// global adjust volume, disable volume, etc
// -----------------------------------------------
#ifndef _KRONO_SOUND_H
#define _KRONO_SOUND_H

#include <allegro.h> // Allegro : http://alleg.sf.net/
#include <aldumb.h>  // DUMB : http://dumb.sf.net/

class CSoundWrapper
{
	public:
		CSoundWrapper();
		~CSoundWrapper();
		
		// digital samples
		int play_sample(const SAMPLE *spl, int vol, int pan, int freq, int loop); // digital sample playing
		void set_volume_d(int v); // volume digital
		int get_volume_d();
	
		// music
		void music_load(DUH *dat); // call this before playing, dat should be a pointer to a music object of a datafile
		
		void music_start(); // start playing
		
		void music_pause(); // pause playback
		void music_resume(); // pause playback
		
		void music_poll(); // _must_ be called at regular intervals to hear the music
		
		void music_stop(); // stop playing
		
		void set_volume_m(int v); // volume music
		int get_volume_m();

		// for all the object
		void set_enabled(bool s);
		bool is_enabled();
		
		// for all the class
		static void global_set_volume(int v);
		static int global_get_volume();
		
		static void global_set_enabled(bool s);
		static bool global_is_enabled();
		
	private:
	// for this object
		bool enabled; // enable this object player (if false, nothing will be played)
		int volume_d; // volume, 0 to 255
		int volume_m; // volume music, 0 to 255

	// for all the class
		static bool global_enabled; // enables the sound system ?
		static int global_volume; // global volume, 0 to 255

		DUH *duh;
		AL_DUH_PLAYER *dp;
};

#endif
