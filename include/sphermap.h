// -----------------------------------------------------------------------
// sphermap.h
// -----------------------------------------------------------------------
// Code to render a sphere 
// From Pixelate #11, article written by Martijn 'amarillion' van Iersel 
// -----------------------------------------------------------------------
// Modified by Kronoman to suit the needs of the game.
// -----------------------------------------------------------------------

#ifndef SPHERMAP_H

#include <allegro.h>

// awful hack to use fixed, seems that my compiler don't like fixed type data :P
#ifndef fixed
	#warning Awful hack in sphermap.h - please remember to check it =)
	#define fixed long int
#endif

void get_planet_rotation_matrix (MATRIX *, fixed , fixed , fixed );
void mapped_sphere_ex (BITMAP *target, int cx, int cy, int r, BITMAP *map, MATRIX *rotmat);

#endif
