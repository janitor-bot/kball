// -----------------------------------------------
// gkernel.h
// -----------------------------------------------
// Game main loop and kernel
// ----------------------------------------------- 
// By Kronoman - July 2003 
// Updated to C++ class in January 2004
// In loving memory of my father
// -----------------------------------------------
#ifndef GKERNEL_H
#define GKERNEL_H

#include <allegro.h>
#include "cball.h" // player ball
#include "tmap.h" // tile map
#include "cwdata.h" // datafile handler
#include "partmang.h" // particle manager
#include "backgbmp.h" // background manager
#include "mytracer.h" // this shit keeps crashing, I need to debug it... dammit!
#include "sound.h" // amazing sound system :o
#include "stats.h" // game statistics
#include "musiclvl.h" // music

// timer update ratio, in BPS
#define GKERNEL_FPSQ 30 

// special flag for when the user aborts the game with ESC
#define GKERNEL_USER_FINISHED_GAME -666


// This class has the game kernel.
class CGameKernel
{
	public:
		CGameKernel();
		~CGameKernel();
		
		// this are the prefered method to start and play a game session
		int play_a_single_level(char *level_filename);
		void play_a_full_campaign(char *level_filename);
		
		// from here, stuff is almost internal only, because they are pretty 'low level'; altough may come handy
		void init(); // initializes game (sets timers, loads data, etc)
		void shutdown(); // shuts down game (unsets timers, unloads data, etc)
		
		int game_loop();  // this is the main game loop; will return CBALL_IS_DEAD, or CBALL_EXIT_LEVEL (dead, or won level)
		int update_logic(); // this updates 1 logic update of game ; will return CBALL_IS_DEAD, CBALL_IS_FINE, or CBALL_EXIT_LEVEL

		void update_screen(); // this updates the screen
		
		void load_level_file(const char *file); // loads a level from a file, and sets the game ready to play on that level -- *MUST* BE CALLED BEFORE STARTING THE GAME LOOP!
		
		BITMAP *tile_bmp_to_screen_size(BITMAP *bmp); // tiles a bitmap to screen size, basically its purpose is to tile the level's background

		CBall player_ball; // ball of the player, I need to touch this from 'outside' ; basically, set the live ammount before each game

		CGameStats stats; // game statistics, I may need to touch them from 'outside'
		
	private:
		
		bool game_over; // game over?
		CTMap game_map; // tile map loaded (with his own tile set)
		CParticleManager particle_manager; // particle manager
		CWDatafile main_data_file; // DATAFILE loaded
		
		BITMAP *dbuffer; // doble buffer
		BITMAP *backdropbmp; // background bitmap for current level
		CBackground background_loader; // loader system for level's background from datafile
		
		
		FONT *game_time_font; // font for showing time left on game
		FONT *game_score_font; // font for showing score on game
		FONT *game_messages_font; // font for showing messages on game

		CSoundWrapper soundw; // sound system
		CMusicLvl music_loader; // loader system for music from datafile

		
		// current level file name (so we can reload it when player loses, etc)
		char current_level_file_name[1024];
		
		// for all the class data
		static int timer_installed_count; // how many times we installed the timer; when this is 0, remove/install timers
		
		// debug tracer
		CMyTracer mtracer; // my tracer to debug this crap
};

#endif

