// ------------------------------------------------------------------
// ui_misc.cpp
// ------------------------------------------------------------------
// This are some miscellaneous helper functions, mainly used by the kernel UI interface
// ------------------------------------------------------------------
// Developed By Kronoman - Copyright (c) 2004
// In loving memory of my father
// ------------------------------------------------------------------

#include "ui_misc.h"

// ------------------------------------------------------------------
// does a cheap interlaced darkening of bmp
// ------------------------------------------------------------------
void ui_misc_dark_bmp(BITMAP *bmp)
{
	for (int yi = 0; yi < SCREEN_H; yi+=2) 
		hline(bmp,0,yi,SCREEN_W,makecol(0,0,0));	
}

// ------------------------------------------------------------------
// text in color of ct with shadow in color of cs
// ------------------------------------------------------------------
void ui_misc_text_out_shadow(BITMAP *bmp, FONT *f, const char *s, int x, int y, int cs, int ct)
{
	textout_centre_ex(bmp,f,s,x+3,y+3,cs,-1);
	textout_centre_ex(bmp,f,s,x,y,ct,-1);
}


// ------------------------------------------------------------------
// waits for input from user, like key/mouse/joystick
// this DON'T PUT the message, just waits!
// ------------------------------------------------------------------
void ui_misc_wait_for_input()
{
	rest(250);
	
	clear_keybuf();

	while ((!keypressed()) && (!mouse_b) && (!joy[0].button[0].b))
	{
		poll_joystick();
		if (keyboard_needs_poll()) poll_keyboard();
		if (mouse_needs_poll()) poll_mouse();
	};	
	
	clear_keybuf();

	rest(250);
}
