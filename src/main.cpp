// -----------------------------------------------
// KBall
// -----------------------------------------------
// By Kronoman
// Copyright (c) 2003, 2004, Kronoman
// In loving memory of my father
// Made in Argentina
// -----------------------------------------------
// main.c
// -----------------------------------------------
// Start up code ; program entry point.
// -----------------------------------------------

#include <allegro.h> // Allegro : http://alleg.sf.net/
#include <aldumb.h>  // DUMB : http://dumb.sf.net/

#include <string.h> // for checking command line

#include "gerror.h" // error reporting
#include "mapedit.h" // built in map editor -:^D
#include "gamemenu.h" // game menu
#include "intro.h"
#include "mytracer.h"

class CMain
{
public:
	CMain()
	{
		mtracer.add("CMain()::CMain()");
	}
	~CMain()
	{
		mtracer.add("CMain()::~CMain()");
	}

	void start(bool want_map_editor);

private:
	CMyTracer mtracer; // debug tracer
	CGameMenu game_menu; // game menu(s)
	CMapEditor map_editor; // map editor
};

void CMain::start(bool want_map_editor)
{
	mtracer.add("CMain()::start()");

	clear_bitmap(screen);

	if (want_map_editor)
	{
		mtracer.add("CMain::start()\n\tStarting map editor");
		map_editor.start_map_editor();
	}
	else
	{
		// DEBUG -- here you can construct a more complex loop, for using different stages
		// like presentation, game menu, and game start, or just a menu, etc

		mtracer.add("CMain::start()\n\tStarting main menu");
		game_menu.do_main_menu(); // main menu
	}
}


// --------------------------------------------------------
// main()
// Program entry point
// --------------------------------------------------------


int main(int argc, char *argv[] )
{
	int i;
	int vid_m = GFX_AUTODETECT_FULLSCREEN; // screen desired graphic mode
	int game_color_depth = -1;  // default color depth, -1 = autodetect from desktop default
	int vid_w = 640; // desired video resolution
	int vid_h = 480;
	bool want_sound = true; // want sound?
	bool want_map_editor = false; // want to use the map editor?
	int desk_bpp = 0;

	CMyTracer mtracer; // debug tracer
	CMyTracer::DISABLE_TRACE = true; // by default, we not debug trace

	allegro_init(); // init allegro
	atexit(&dumb_exit); // DUMB exit functions

	// check command line parameters
	for (i=1; i < argc; i++)
	{
		if (stricmp(argv[i], "-trace") == 0)
			CMyTracer::DISABLE_TRACE = false; // enable debug tracer

		if (stricmp(argv[i], "-wn") == 0)
			vid_m = GFX_AUTODETECT_WINDOWED;

		if (stricmp(argv[i], "-nosound") == 0)
			want_sound = false;

		if (stricmp(argv[i], "-w") == 0)
			vid_m = GFX_AUTODETECT_WINDOWED;

		if (stricmp(argv[i], "-bpp16") == 0)
			game_color_depth = 16;

		if (stricmp(argv[i], "-bpp15") == 0)
			game_color_depth = 15;

		if (stricmp(argv[i], "-bpp32") == 0)
			game_color_depth = 32;

		if (stricmp(argv[i], "-bpp24") == 0)
			game_color_depth = 24;

		if (stricmp(argv[i], "-bpp8") == 0)
		{
			raise_error("main() : Sorry, this program don't support 8 bpp displays.\nThis program needs a true color display at %3d x %3d resolution.\nTip: Try removing the -bpp8 switch from the command line invocation.",  vid_w, vid_h);
		}

		// development options
		if (stricmp(argv[i], "-mapeditor") == 0)
			want_map_editor = true; // use the map editor... cool
	}
	
	// dumb_register_packfiles(); // DUMB will read from packfiles - NO NECESARIO, no cargo nada del disco, solo a traves de datafiles
	
	// register DUMB music files -- DEBUG : registrar solo el formato que voy a usar!
	dumb_register_dat_it(DUMB_DAT_IT);
	dumb_register_dat_xm(DUMB_DAT_XM);
	dumb_register_dat_s3m(DUMB_DAT_S3M);
	dumb_register_dat_mod(DUMB_DAT_MOD);
	
	set_window_title("KBall"); // setear nombre de la ventana :o

	mtracer.add("\n--------------------------------------------------------\nmain() started\n");

	desk_bpp = desktop_color_depth(); // using the same color depth as the host will make the game run faster
	if (desk_bpp != 8 && desk_bpp != 0 && game_color_depth == -1)
	{
		// use the color depth of desktop
		game_color_depth = desk_bpp;
	}

	if (game_color_depth < 8)
		game_color_depth = 16; // safe check

	srand(time(NULL)); // init random numbers

	if (install_timer() != 0)
		raise_error("main() : can't install timer driver");

	mtracer.add("\tTimer installed");

	if (install_keyboard() != 0)
		raise_error("main() : can't install keyboard driver");

	install_mouse();

	install_joystick(JOY_TYPE_AUTODETECT);

	mtracer.add("\tInput devices installed");

	if (want_sound)
	{
		//if (install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, NULL) ) raise_error("install_sound() failed.\n'%s'\nTry disabling the sound with -nosound parameter.\n", allegro_error);

		reserve_voices(8, 0);
		set_volume_per_voice(2); // warning - this may cause distortion

		mtracer.add("\tInstall sound");
		if (!install_sound(DIGI_AUTODETECT, MIDI_NONE, NULL) )
			mtracer.add("\t\tSound system OK!\n" );
		else
			mtracer.add("\t\tSound system FAILED! : %s", allegro_error);

		set_volume(255,-1);
		//set_hardware_volume(255,-1);
	}


	// set graphics mode
	set_color_depth(game_color_depth);

	mtracer.add("\tTrying to run in %3dx%3d@%2d bpp",vid_w, vid_h, game_color_depth);

	if ( set_gfx_mode(vid_m, vid_w, vid_h, 0, 0) )
	{
		set_color_depth(16);
		if ( set_gfx_mode(vid_m, vid_w, vid_h, 0, 0) )
		{
			set_color_depth(15);
			if ( set_gfx_mode(vid_m, vid_w, vid_h, 0, 0) )
			{
				set_color_depth(32);
				if ( set_gfx_mode(vid_m, vid_w, vid_h, 0, 0) )
				{
					set_color_depth(24);
					if ( set_gfx_mode(vid_m, vid_w, vid_h, 0, 0) )
					{
						raise_error("main() : I can't set the graphics mode (%3d x %3d @ %2d bpp)\nI also tried with 16 bpp, 15 bpp, 32 bpp and 24 bpp\n", vid_w, vid_h, game_color_depth);
					}
				}
			}
		}
	}

	set_color_conversion(COLORCONV_TOTAL | COLORCONV_KEEP_TRANS);
	// I need software 3D code for this game, so I init the 3D scene system of Allegro - DEBUG
	mtracer.add("Starts 3D software rendered scene -> create_scene()");
	create_scene(4800,2000); // max n of edges and polygons to render - DEBUG - take care of this to see if the calculus is right

	mtracer.add("-- Allegro start up code done --\n\nStarting CMain class");

	kball_do_the_intro(); // INTRO OF GAME
	
	textout_centre_ex(screen, font, "[   Please wait... loading...   ]", SCREEN_W/2, SCREEN_H/2, makecol(255,255,255), makecol(0,0,64));

	CMain *cmain;
	cmain = new(CMain);
	cmain->start(want_map_editor);
	delete(cmain);

	kball_do_the_exit(); // exit of game
	
	mtracer.add("\nmain() finished");

	// Release memory used by the 3D code -- DEBUG
	mtracer.add("Stops 3D software rendered scene -> destroy_scene()");
	destroy_scene();

	return 0; // normal end of the program
}
END_OF_MAIN();
