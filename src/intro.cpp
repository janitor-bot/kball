// ------------------------------------------------------------------
// intro.cpp
// ------------------------------------------------------------------
// This is the intro and exit secuence for the game - everything hardcoded, sorry
// ------------------------------------------------------------------
// By Kronoman - In loving memory of my father
// Copyright (c) 2004, Kronoman
// ------------------------------------------------------------------

#include <allegro.h>
#include "intro.h"
#include "cwdata.h"

void kball_do_the_intro()
{
	CWDatafile data;

	clear_bitmap(screen);
	textout_centre_ex(screen, font, "[   Please wait... loading...   ]", SCREEN_W / 2, SCREEN_H / 2, makecol(255, 255, 255), makecol(0, 0, 64));
	
	data.load_datafile("intro.dat");

	BITMAP *logo = (BITMAP *)data.get_resource_dat("LOGO_KRONOMAN_BMP");
	
	SAMPLE *krono_wav = (SAMPLE *)data.get_resource_dat("KRONOMAN_WAV");

	clear_bitmap(screen);

	blit(logo, screen,0,0,0,0,logo->w,logo->h);

	rest(1000);

	play_sample(krono_wav, 255,128,1000,0);

	rest(3000);
	
	clear_bitmap(screen);
	data.nuke_datafile();	
}

void kball_do_the_exit()
{
	char tmp_str[2048];
	BITMAP *face = NULL;
	CWDatafile data;
	DATAFILE *datastream; // data stream for reading about text
	char *readstream; // stream for reading about text
	int xstream = 0; // where I'm reading the stream?

	clear_bitmap(screen);
	textout_centre_ex(screen, font, "[   Please wait... loading...   ]", SCREEN_W / 2, SCREEN_H / 2, makecol(255, 255, 255), makecol(0, 0, 64));
	
	data.load_datafile("intro.dat");

	datastream = data.get_resource("INTRO_TXT"); // get about text

	readstream = (char *)datastream->dat;
	
	usprintf(tmp_str, "%d_BMP", rand()%5+1);
	face = (BITMAP *)data.get_resource_dat(tmp_str);

	clear_to_color(screen, makecol(255,255,255));
	blit(face, screen,0,0,(SCREEN_W - face->w) / 2, 5,face->w,face->h);
	// poner texto
	int y = face->h+5;
	int x = SCREEN_W / 2;
	int c = makecol(0,0,0);
	int c2 = makecol(0,0,0);
	char buf[1024];
	int xbuf = 0;
	for (xstream =0; xstream < datastream->size; xstream++)
	{
		if (readstream[xstream] > '\n')
		{
			buf[xbuf] = readstream[xstream];
			xbuf++;
		}
		else
		{
			if (readstream[xstream] == '\n')
			{
				buf[xbuf] = '\0';
				//textout_centre_ex(screen, font, buf, x+1,y+1,c2,-1);
				textout_centre_ex(screen, font, buf, x,y,c,-1);
				
				xbuf = 0;
				x = SCREEN_W/2;
				y += text_height(font);
			}
		}
	}
	buf[xbuf] = '\0';
	textout_centre_ex(screen, font, buf, x,y,c,-1);
	
	rest(5000);
	clear_bitmap(screen);
}
